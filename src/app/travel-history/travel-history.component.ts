import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from '../services/api.service';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { take } from 'rxjs';
import { TravelHistory } from '../interfaces/TravelHistory';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';


@Component({
  selector: 'app-travel-history',
  standalone: true,
  imports: [CommonModule, MatToolbarModule, MatIconModule, MatButtonModule, MatListModule, RouterModule],
  templateUrl: './travel-history.component.html',
  styleUrl: './travel-history.component.scss'
})
export class TravelHistoryComponent {
  travelhistory: TravelHistory[] = [];
  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.apiService.getTravelHistory().pipe(
      take(1)
    ).subscribe({
      next: response => {
        this.travelhistory = response.reverse();
      },
      error: e => console.error(e)
    });
  }

  formatDate(s: string) {
    const date = new Date(s);
    return date.toLocaleString();
  }
}

