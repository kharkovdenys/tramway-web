import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { take } from 'rxjs';


@Component({
  selector: 'app-login',
  standalone: true,
  imports: [CommonModule, MatCardModule, FormsModule, MatButtonModule, MatFormFieldModule, MatInputModule],
  providers: [ApiService],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  public loginValid = true;
  public username = '';
  public password = '';

  constructor(
    private router: Router,
    private apiService: ApiService
  ) { }

  public onSubmit(): void {
    this.loginValid = true;

    this.apiService.login(this.username, this.password).pipe(
      take(1)
    ).subscribe({
      next: _ => {
        this.loginValid = true;
        this.router.navigateByUrl('/main');
      },
      error: _ => this.loginValid = false
    });
  }
}
