import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl = 'http://localhost:3001';

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<{ token: string }> {
    const url = `${this.apiUrl}/login`;
    const credentials = { username, password };

    return this.http.post<{ token: string }>(url, credentials)
      .pipe(
        tap(response => this.setToken(response.token))
      );
  }

  private setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  getToken(): string | null {
    return localStorage.getItem('token');
  }

  isLoggedIn(): boolean {
    return !!this.getToken();
  }

  logout(): void {
    localStorage.removeItem('token');
  }

  getInfo(): Observable<any> {
    const url = `${this.apiUrl}/info`;

    const headers = new HttpHeaders({
      'Authorization': `${this.getToken()}`
    });

    return this.http.get(url, { headers });
  }

  getTravelHistory(): Observable<any> {
    const url = `${this.apiUrl}/travel_history`;

    const headers = new HttpHeaders({
      'Authorization': `${this.getToken()}`
    });

    return this.http.get(url, { headers });
  }

  getTransaction(): Observable<any> {
    const url = `${this.apiUrl}/transaction_history`;

    const headers = new HttpHeaders({
      'Authorization': `${this.getToken()}`
    });

    return this.http.get(url, { headers });
  }

  getRoutes(): Observable<any> {
    const url = `${this.apiUrl}/routes`;

    const headers = new HttpHeaders({
      'Authorization': `${this.getToken()}`
    });

    return this.http.get(url, { headers });
  }

  getQR(): Observable<any> {
    const url = `${this.apiUrl}/my_qr`;

    const headers = new HttpHeaders({
      'Authorization': `${this.getToken()}`
    });

    return this.http.get(url, { headers });
  }

  getSchedule(id: string): Observable<any> {
    const url = `${this.apiUrl}/schedule/${id}`;

    const headers = new HttpHeaders({
      'Authorization': `${this.getToken()}`
    });

    return this.http.get(url, { headers });
  }

  buyQR(): Observable<any> {
    const url = `${this.apiUrl}/qr`;

    const headers = new HttpHeaders({
      'Authorization': `${this.getToken()}`
    });

    return this.http.get(url, { headers });
  }

  topUp(add: number, type_id: number): Observable<any> {
    const url = `${this.apiUrl}/top_up`;

    const headers = new HttpHeaders({
      'Authorization': `${this.getToken()}`
    });

    return this.http.post(url, { add, type_id }, { headers });
  }
}
