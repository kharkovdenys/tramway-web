import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from '../services/api.service';
import { take } from 'rxjs';
import { Travelcard } from '../interfaces/Travelcard';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-main',
  standalone: true,
  imports: [CommonModule, MatCardModule, MatButtonModule, MatGridListModule, MatIconModule, RouterModule],
  providers: [ApiService],
  templateUrl: './main.component.html',
  styleUrl: './main.component.scss'
})
export class MainComponent {
  travelcard?: Travelcard;
  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.apiService.getInfo().pipe(
      take(1)
    ).subscribe({
      next: response => {
        this.travelcard = response[0];
      },
      error: e => console.error(e)
    });
  }
}

