import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { ApiService } from '../services/api.service';
import { take } from 'rxjs';
import { MatListModule } from '@angular/material/list';
import { Route } from '../interfaces/Route';

@Component({
  selector: 'app-routes',
  standalone: true,
  imports: [CommonModule, MatToolbarModule, MatIconModule, RouterModule, MatButtonModule, MatListModule],
  templateUrl: './routes.component.html',
  styleUrl: './routes.component.scss'
})
export class RoutesComponent {
  routes: Route[] = [];
  constructor(
    private apiService: ApiService
  ) { }
  ngOnInit(): void {
    this.apiService.getRoutes().pipe(
      take(1)
    ).subscribe({
      next: response => {
        this.routes = response;
      },
      error: e => console.error(e)
    });
  }
}
