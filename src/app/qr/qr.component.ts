import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCarouselModule } from '@magloft/material-carousel';
import { ApiService } from '../services/api.service';
import { take } from 'rxjs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { QR } from '../interfaces/QR';
import { NgxQrcodeStylingModule } from 'ngx-qrcode-styling';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-qr',
  standalone: true,
  imports: [CommonModule, MatCarouselModule, MatToolbarModule, MatIconModule, MatButtonModule, RouterModule, NgxQrcodeStylingModule],
  templateUrl: './qr.component.html',
  styleUrl: './qr.component.scss'
})
export class QrComponent {
  qrs: QR[] = [];
  constructor(
    private apiService: ApiService, private toastrService: ToastrService
  ) { }
  ngOnInit(): void {
    this.apiService.getQR().pipe(
      take(1)
    ).subscribe({
      next: response => {
        this.qrs = response.reverse();
      },
      error: e => console.error(e)
    });
  }

  formatDate(s: string) {
    const date = new Date(s);
    return date.toLocaleString();
  }

  buy() {
    this.apiService.buyQR().pipe(
      take(1)
    ).subscribe({
      next: _ => {
        window.location.reload();
      },
      error: _ => this.toastrService.error('Недостатньо коштів')
    });
  }
}
