import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { authGuard } from './auth.guard';
import { MainComponent } from './main/main.component';
import { TravelHistoryComponent } from './travel-history/travel-history.component';
import { TransactionComponent } from './transaction/transaction.component';
import { QrComponent } from './qr/qr.component';
import { RoutesComponent } from './routes/routes.component';
import { ScheduleComponent } from './schedule/schedule.component';

export const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'travel_history', component: TravelHistoryComponent, canActivate: [authGuard] },
    { path: 'transaction', component: TransactionComponent, canActivate: [authGuard] },
    { path: 'qr_code', component: QrComponent, canActivate: [authGuard] },
    { path: 'routes', component: RoutesComponent, canActivate: [authGuard] },
    { path: 'schedule/:id', component: ScheduleComponent, canActivate: [authGuard] },
    { path: '**', component: MainComponent, canActivate: [authGuard] }
];
