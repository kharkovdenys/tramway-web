import { take } from 'rxjs';

import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { Schedule } from '../interfaces/Schedule';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-schedule',
  standalone: true,
  imports: [CommonModule, MatToolbarModule, MatIconModule, MatButtonModule, RouterModule, MatListModule, FormsModule, MatCheckboxModule],
  templateUrl: './schedule.component.html',
  styleUrl: './schedule.component.scss'
})
export class ScheduleComponent {
  schedules: Schedule[] = [];
  reverse: boolean = false;
  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute
  ) { }
  ngOnInit(): void {
    this.apiService.getSchedule(this.route.snapshot.paramMap.get('id') || '').pipe(
      take(1)
    ).subscribe({
      next: response => {
        this.schedules = response.sort((a: Schedule, b: Schedule) => a.number - b.number);;
      },
      error: e => console.error(e)
    });
  }

  changeMode() {
    if (!this.reverse)
      this.schedules = this.schedules.sort((a: Schedule, b: Schedule) => a.number - b.number);
    else {
      this.schedules = this.schedules.sort((a: Schedule, b: Schedule) => b.number - a.number);
    }
  }
}
