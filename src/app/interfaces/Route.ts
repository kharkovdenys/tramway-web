export interface Route {
    Duration: number;
    Route_id: number;
    Route_number: string;
    Stops: number;
}
