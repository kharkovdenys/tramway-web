export interface TravelHistory {
    Date_of_start: string;
    Reverse: boolean;
    Travel_id: number;
    routeEnd: { stop: { Stop_name_ua?: string } },
    routeStart: { stop: { Stop_name_ua: string } },
}
