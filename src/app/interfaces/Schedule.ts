export interface Schedule {
    stop: { Stop_name_ua: string },
    number: number,
    arrivalTime: string[],
    arrivalTimeReverse: string[]
}
