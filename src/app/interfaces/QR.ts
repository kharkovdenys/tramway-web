export interface QR {
    GUID: string;
    Used: boolean;
    Validity: string;
}
