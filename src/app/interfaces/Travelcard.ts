export interface Travelcard {
    Balance: number;
    First_name: string;
    IPN: string;
    Last_name: string;
    Travelcard_id: number;
    Type_card_id: number;
    type_card: {
        Type_card_id: number;
        Type_name_en: string;
        Type_name_ua: string;
    }
}
