export interface Transaction {
    Change: number;
    Date: string;
    Transaction_id: number;
    Travelcard_id: number;
    Type: { Type_id: number, Type_name_ua: string, Type_name_en: string }
    Type_id: number;
}
