import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from '../services/api.service';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { take } from 'rxjs';
import { Transaction } from '../interfaces/Transaction';
import { RouterModule } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
} from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';

export interface DialogData {
  money: number;
}

@Component({
  selector: 'app-transaction',
  standalone: true,
  imports: [CommonModule, MatToolbarModule, MatIconModule, MatButtonModule, RouterModule, MatListModule, MatCardModule],
  templateUrl: './transaction.component.html',
  styleUrl: './transaction.component.scss'
})
export class TransactionComponent {
  transactions: Transaction[] = [];

  constructor(
    private apiService: ApiService,
    public dialog: MatDialog
  ) { }
  ngOnInit(): void {
    this.apiService.getTransaction().pipe(
      take(1)
    ).subscribe({
      next: response => {
        this.transactions = response.reverse();
      },
      error: e => console.error(e)
    });
  }

  formatDate(s: string) {
    const date = new Date(s);
    return date.toLocaleString();
  }

  balance() {
    return this.transactions.reduce((accumulator, currentValue) => accumulator + currentValue.Change, 0).toFixed(2);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(Dialog, {
      data: { money: 0 },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.apiService.topUp(result, 4).pipe(
        take(1)
      ).subscribe({
        next: _ => {
          window.location.reload();
        },
        error: e => console.error(e)
      });
    });
  }
}

@Component({
  selector: 'dialog-money',
  templateUrl: './dialog-money.html',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
  ],
})
export class Dialog {
  constructor(
    public dialogRef: MatDialogRef<Dialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

